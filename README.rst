vim-git-grep-prg
================

Installation
------------

Copy it somewhere sensible.  I keep mine in ``~/bin/``

Configuration
-------------

Add this to your ``~/.vimrc`` (for vim) or ``~/.config/nvim/init.vim`` (for
neovim)::

    set grepprg=~/bin/vim-git-grep-prg

Use
---

Now the vim ``:grep`` command will automatically decide whether to grep using
``grep`` or ``git grep``.  It does this by deciding if the arguments seem to
make sense to pass to ``grep`` (i.e. there's at least one filename or ``-r``
is used).

You can force ``git grep`` to be run by passing ``--git`` as a command line
option (which will be removed before passing the other arguments to ``git
grep``) - e.g. this is useful if you want to restrict the ``git grep`` to
a subdirectory.
